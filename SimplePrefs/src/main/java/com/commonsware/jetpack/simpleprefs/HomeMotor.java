/*
  Copyright (c) 2019 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Android Jetpack_

  https://commonsware.com/Jetpack
*/

package com.commonsware.jetpack.simpleprefs;

import android.app.Application;
import android.content.SharedPreferences;
import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.preference.PreferenceManager;

public class HomeMotor extends AndroidViewModel {
  private final SharedPreferences prefs;
  private final MutableLiveData<HomeViewState> states = new MutableLiveData<>();

  public HomeMotor(@NonNull Application application) {
    super(application);

    prefs = PreferenceManager.getDefaultSharedPreferences(application);
    prefs.registerOnSharedPreferenceChangeListener(LISTENER);
    emitState();
  }

  @Override
  protected void onCleared() {
    prefs.unregisterOnSharedPreferenceChangeListener(LISTENER);
  }

  LiveData<HomeViewState> getStates() {
    return states;
  }

  private void emitState() {
    states.setValue(
      new HomeViewState(prefs.getBoolean("checkbox", false),
        prefs.getString("field", ""), prefs.getString("list", "")));
  }

  private SharedPreferences.OnSharedPreferenceChangeListener LISTENER =
    (prefs, key) -> emitState();
}

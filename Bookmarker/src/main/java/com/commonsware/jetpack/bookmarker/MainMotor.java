/*
  Copyright (c) 2019 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Android Jetpack_

  https://commonsware.com/Jetpack
*/

package com.commonsware.jetpack.bookmarker;

import android.app.Application;
import java.util.ArrayList;
import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.Transformations;

public class MainMotor extends AndroidViewModel {
  private final BookmarkRepository repo;
  private MediatorLiveData<Event<BookmarkResult>> saveEvents = new MediatorLiveData<>();
  private LiveData<Event<BookmarkResult>> lastSave;
  final LiveData<MainViewState> states;

  public MainMotor(@NonNull Application application) {
    super(application);

    repo = BookmarkRepository.get(application);
    states = Transformations.map(repo.load(),
      models -> {
        ArrayList<RowState> content = new ArrayList<>();

        for (BookmarkModel model : models) {
          content.add(new RowState(model));
        }

        return new MainViewState(content);
      });
  }

  LiveData<Event<BookmarkResult>> getSaveEvents() {
    return saveEvents;
  }

  void save(String pageUrl) {
    saveEvents.removeSource(lastSave);
    lastSave = Transformations.map(repo.save(pageUrl), Event::new);
    saveEvents.addSource(lastSave, event -> saveEvents.setValue(event));
  }
}

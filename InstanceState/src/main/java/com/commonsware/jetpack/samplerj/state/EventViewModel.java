/*
  Copyright (c) 2018-2020 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Android Jetpack_

  https://commonsware.com/Jetpack
*/

package com.commonsware.jetpack.samplerj.state;

import android.os.SystemClock;
import java.util.ArrayList;
import java.util.Random;
import androidx.lifecycle.SavedStateHandle;
import androidx.lifecycle.ViewModel;

public class EventViewModel extends ViewModel {
  private static final String STATE_EVENTS = "events";
  private static final String STATE_START_TIME = "startTime";

  final ArrayList<Event> events;
  final Long startTime;
  private final int id = new Random().nextInt();
  private final SavedStateHandle state;

  public EventViewModel(SavedStateHandle state) {
    this.state = state;

    ArrayList<Event> events = state.get(STATE_EVENTS);

    if (events == null) {
      this.events = new ArrayList<>();
    }
    else {
      this.events = events;
    }

    Long startTime = state.get(STATE_START_TIME);

    if (startTime == null) {
      this.startTime = SystemClock.elapsedRealtime();
      state.set(STATE_START_TIME, this.startTime);
    }
    else {
      this.startTime = startTime;
    }
  }

  void addEvent(String message, int activityHash) {
    events.add(new Event(message, activityHash, id));
    state.set(STATE_EVENTS, events);
  }

  @Override
  protected void onCleared() {
    events.clear();
  }
}
